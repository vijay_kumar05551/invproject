<?php

/** This class is made by Vijay Kumar for Geetu in 9//11/2018
*
 * In this class user will get list of Customers name
 */

class Customers
{
  // Get List of Customers Names
  public function getCustomersName()
  {
    // Variables
    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "project_inv";
    $get_c_name = "customer_name";
    $connection_error_msg = "Connection failed: ";

    // Create connection with DB
    $conn = new mysqli($servername, $username, $password, $dbname);

    // Check connection
    if ($conn->connect_error) {
      die($connection_error_msg . $conn->connect_error);
    }
    else {
      $sql = "SELECT customer_name FROM invoice";
      $result = $conn->query($sql);
      if ($result->num_rows > 0) {
        // output data of each row
        while($row = $result->fetch_assoc()) {
          echo $row[$get_c_name];
          return $row[$get_c_name];
        }
      } else {
          echo "0 results";
        }
        $conn->close();
    }
  }
}

$customer = new Customers();
$customer->getCustomersName();

 ?>
